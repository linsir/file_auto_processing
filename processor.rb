require_relative 'log/log'
require_relative 'config/config'

class Processor
  def self.process_cs_file(path)
    begin
      text = File.read(path)
      if text.match(Config::REGEXP_PATTERN)
        get = text.gsub(Config::REGEXP_PATTERN) {|sym| "[DataMember(Name=\"#{sym.match(Config::REGEXP_PATTERN).captures.first || "" }\"\)\]"}
        if get != nil
          File.open(path, "w+") do |file|
            file << get
          end

        end
      end
    rescue StandardError => e
      Log.log_error("#{e.message}")
    end
  end
end
