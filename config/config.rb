module Config
  TARGET_DIR = 'D:\Downloads\test\XServiceWebApi_Ruby_Playground'
  ACCEPTED_FORMATS = [".cs", ".asax"]
  LOG_FILE = "log.txt"
  REGEXP_PATTERN = /\[BsonElement\(\"(\w*)\"\)\]/
end
