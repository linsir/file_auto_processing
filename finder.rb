require_relative 'log/log'
require_relative 'config/config'
require_relative 'processor'

class Finder

  def self.search_sub_dir(parent_dir)
    Log.debug("step into #{parent_dir}")
    sub_dirs = Dir.children(parent_dir)

    if sub_dirs.count == 0
      Log.debug("#{parent_dir} has none")
    end

    sub_dirs.each do |item|
      path = File.join(parent_dir, item).gsub(File::SEPARATOR, File::ALT_SEPARATOR || File::SEPARATOR)
      if File.directory?(path)
        search_sub_dir(path)
      elsif File.file?(path) && Config::ACCEPTED_FORMATS.include?(File.extname(item))
        Processor.process_cs_file(path)
      end
    end

  end
end
