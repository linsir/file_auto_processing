require 'logger'
require_relative '../config/config'

class Log
  def self.init()
    @@log ||= Logger.new(File.open(Config::LOG_FILE, "a+"))
    @@log.formatter ||= proc do |severity, datetime, progname, msg|
      date_format = datetime.strftime("%Y-%m-%d %H:%M:%S")
      if severity == "INFO" or severity == "WARN"
        "[#{date_format}] #{severity}  (#{progname}): #{msg}\n"
      else
        "[#{date_format}] #{severity} (#{progname}): #{msg}\n"
      end
    end
    @@init = true
  end

  def self.need_init()
    @@init ||= false
    return !@@init
  end

  def self.log_error(msg)
    if need_init()
      init()
    end
    @@log.error(msg)
  end

  def self.debug(msg)
    if need_init()
      init()
    end
    @@log.info(msg)
  end
end